const error = {
  "error": "some error"
}

const success = {
  "success": "submitted successfully"
}

const stock = [
  { product: 'coffee', stock: 30 },
  { product: 'tea', stock: 26 },
  { product: 'milk', stock: 30 },
  { product: 'sugar', stock: 30 },
]

const temperatures = [
  {
    "temps": [
      {
        "timestamp": 1576668793061,
        "temp": 99
      },
      {
        "timestamp": 1576668793100,
        "temp": 97
      },
      {
        "timestamp": 1578614076363,
        "temp": 99
      },
      {
        "timestamp": 1578614081031,
        "temp": 92
      },
      {
        "timestamp": 1578614086034,
        "temp": 100
      },
      {
        "timestamp": 1578614091037,
        "temp": 95
      },
      {
        "timestamp": 1578614096034,
        "temp": 91
      },
      {
        "timestamp": 1578614101036,
        "temp": 93
      },
      {
        "timestamp": 1578614106039,
        "temp": 95
      },
      {
        "timestamp": 1578614111041,
        "temp": 91
      },
      {
        "timestamp": 1578614116039,
        "temp": 99
      },
      {
        "timestamp": 1578614121042,
        "temp": 93
      },
      {
        "timestamp": 1578614126042,
        "temp": 93
      },
      {
        "timestamp": 1578614131042,
        "temp": 92
      },
      {
        "timestamp": 1578614136039,
        "temp": 93
      },
      {
        "timestamp": 1578614141042,
        "temp": 99
      },
      {
        "timestamp": 1578614146043,
        "temp": 95
      },
      {
        "timestamp": 1578614151041,
        "temp": 93
      },
      {
        "timestamp": 1578614156043,
        "temp": 91
      },
      {
        "timestamp": 1578614161039,
        "temp": 99
      },
      {
        "timestamp": 1578614166042,
        "temp": 100
      },
      {
        "timestamp": 1578614171043,
        "temp": 98
      },
      {
        "timestamp": 1578614176044,
        "temp": 94
      },
      {
        "timestamp": 1578614181044,
        "temp": 98
      },
      {
        "timestamp": 1578614186044,
        "temp": 98
      },
      {
        "timestamp": 1578614191041,
        "temp": 97
      },
      {
        "timestamp": 1578614196044,
        "temp": 99
      },
      {
        "timestamp": 1578614201041,
        "temp": 99
      },
      {
        "timestamp": 1578614206044,
        "temp": 96
      },
      {
        "timestamp": 1578614211041,
        "temp": 93
      },
      {
        "timestamp": 1578614216044,
        "temp": 100
      },
      {
        "timestamp": 1578614221041,
        "temp": 93
      }
    ]
  }
]

export {
  error,
  success,
  stock,
  temperatures,
}