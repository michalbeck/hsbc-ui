import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Dispenser from 'components/Dispenser'

const defaultProps = {
  onDispense: jest.fn()
}

describe('<Dispenser />', () => {
  describe('@render', () => {
    it('renders with default props', () => {
      const wrapper = shallow(<Dispenser {...defaultProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('input[type="radio"]')).toHaveLength(2)
      expect(wrapper.find('input[type="checkbox"]')).toHaveLength(2)
      expect(wrapper.find('button')).toHaveLength(1)
      expect(wrapper.find('.Dispenser__message')).toHaveLength(1)
      expect(wrapper.find('.Dispenser__message').hasClass('Dispenser__message--hidden')).toBe(true)
    })
  })

  describe('@events', () => {
    it('handles radio button selection', () => {
      const wrapper = shallow(<Dispenser {...defaultProps} />)
      expect(wrapper.find('input[type="radio"]').at(0).props().checked).toBe(false)
      expect(wrapper.find('input[type="radio"]').at(1).props().checked).toBe(false)
      expect(wrapper.find('button').props().disabled).toBe(true)
      wrapper.find('input[type="radio"]').at(0).props().onChange({ target: { value: 'coffee' }})
      expect(wrapper.find('input[type="radio"]').at(0).props().checked).toBe(true)
      expect(wrapper.find('input[type="radio"]').at(1).props().checked).toBe(false)
      expect(wrapper.find('button').props().disabled).toBe(false)
      wrapper.find('input[type="radio"]').at(1).props().onChange({ target: { value: 'tea' }})
      expect(wrapper.find('input[type="radio"]').at(0).props().checked).toBe(false)
      expect(wrapper.find('input[type="radio"]').at(1).props().checked).toBe(true)
      expect(wrapper.find('button').props().disabled).toBe(false)
    })
    it('handles checkbox selection', () => {
      const wrapper = shallow(<Dispenser {...defaultProps} />)
      expect(wrapper.find('input[type="checkbox"]').at(0).props().checked).toBe(false)
      wrapper.find('input[type="checkbox"]').at(0).props().onChange({ target: { value: 'milk', checked: true }})
      expect(wrapper.find('input[type="checkbox"]').at(0).props().checked).toBe(true)
      expect(wrapper.find('input[type="checkbox"]').at(1).props().checked).toBe(false)
      wrapper.find('input[type="checkbox"]').at(1).props().onChange({ target: { value: 'sugar', checked: true }})
      expect(wrapper.find('input[type="checkbox"]').at(1).props().checked).toBe(true)
    })
    it('invokes onDispense action when dispense button clicked', () => {
      const wrapper = shallow(<Dispenser {...defaultProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      wrapper.find('input').at(0).props().onChange({ target: { value: 'coffee' }})
      wrapper.find('input[type="checkbox"]').at(0).props().onChange({ target: { value: 'milk', checked: true }})
      expect(wrapper.find('button').props().disabled).toBe(false)
      wrapper.find('button').props().onClick()
      expect(defaultProps.onDispense).toHaveBeenCalledTimes(1)
      expect(defaultProps.onDispense).toHaveBeenCalledWith(['coffee', 'milk'])
    })
    it('handles dispensing drink message display', () => {
      jest.useFakeTimers()
      jest.spyOn(React, 'useEffect').mockImplementation(f => f())
      const wrapper = shallow(<Dispenser {...defaultProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      wrapper.find('input').at(0).props().onChange({ target: { value: 'coffee' }})
      expect(wrapper.find('.Dispenser__message').text()).toEqual('Dispensing coffee...')
      wrapper.find('input[type="checkbox"]').at(0).props().onChange({ target: { value: 'milk', checked: true }})
      expect(wrapper.find('.Dispenser__message').text()).toEqual('Dispensing coffee with milk...')
      expect(wrapper.find('.Dispenser__message').hasClass('Dispenser__message--hidden')).toBe(true)
      wrapper.find('button').props().onClick()
      expect(wrapper.find('.Dispenser__message').hasClass('Dispenser__message--hidden')).toBe(false)
      jest.advanceTimersByTime(2000)
      expect(wrapper.find('.Dispenser__message').hasClass('Dispenser__message--hidden')).toBe(true)
    })
  })
})
