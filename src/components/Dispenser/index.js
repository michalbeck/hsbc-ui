import React, { useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { without } from 'ramda'

import './Dispenser.css'

const Dispenser = ({ onDispense }) => {
  const [drink, setDrink] = useState(null)
  const [extras, setExtras] = useState([])
  const [dispensing, setDispensing] = useState(false)

  React.useEffect(() => {
    if (dispensing) setTimeout(() => resetDrink(), 2000)
  }, [dispensing])

  const handleChangeDrink = ({ target }) => {
    const { value } = target
    setDrink(value)
  }

  const handleChangeExtras = ({ target }) => {
    const { checked, value } = target
    setExtras(
      checked
        ? [...extras, value]
        : without([value], extras)
    )
  }

  const handleDispense = () => {
    const products = [ drink, ...extras ]
    onDispense(products)
    setDispensing(true)
  }

  const resetDrink = () => {
    setDrink(null)
    setExtras([])
    setDispensing(false)
  }

  return (
    <div className='Dispenser'>
      <h3>Choose drink:</h3>
      <div className='Dispenser__drinks'>
        <label>
          <input
            type='radio'
            name='drinks'
            value='coffee'
            checked={drink === 'coffee'}
            onChange={handleChangeDrink}
          />
          <span>Coffee</span>
        </label>
        <label>
          <input
            type='radio'
            name='drinks'
            value='tea'
            checked={drink === 'tea'}
            onChange={handleChangeDrink}
          />
          <span>Tea</span>
        </label>
      </div>
      <h3>Add extras:</h3>
      <div className='Dispenser__extras'>
        <label>
          <input
            type='checkbox'
            value='milk'
            checked={extras.includes('milk')}
            onChange={handleChangeExtras}
          />
          <span>Milk</span>
        </label>
        <label>
          <input
            type='checkbox'
            value='sugar'
            checked={extras.includes('sugar')}
            onChange={handleChangeExtras}
          />
          <span>Sugar</span>
        </label>
      </div>
      <button onClick={handleDispense} disabled={!drink}>Dispense drink</button>
      <p className={classNames('Dispenser__message', { 'Dispenser__message--hidden': !dispensing })}>
        { extras.length
            ? `Dispensing ${drink} with ${[...extras].join(', ')}...`
            : `Dispensing ${drink}...`
        }
      </p>
    </div>
  )
}

Dispenser.propTypes = {
  onDispense: PropTypes.func,
}

export default Dispenser
