import React from 'react'
import PropTypes from 'prop-types'

import './Maintenance.css'

const Maintenance = ({ products, temperatures, fetchTemperatures }) => {
  return (
    <div className='Maintenance'>
      <h3>Maintenance</h3>
      <div className='Maintenance__products'>
      { products.length ? (
          <table>
            <thead>
              <tr>
                <th>Product</th>
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
              {
                products.map(({ product, stock }) => (
                  <tr key={product} className='Maintenance__product'>
                    <td>{product}</td>
                    <td>{stock}</td>
                  </tr>
                ))
              }
            </tbody>
          </table>
      ) : null }
      </div>

      <div className='Maintenance__temperatures'>
        <button onClick={fetchTemperatures}>Fetch temperatures</button>
        { temperatures.length ? (
          <table>
            <thead>
              <tr>
                <th />
                <th>Temperature</th>
                <th>Timestamp</th>
              </tr>
            </thead>
            <tbody>
              {
                temperatures.map(({ timestamp, temp }, index) => (
                  <tr key={timestamp} className='Maintenance__temperature'>
                    <td>{index + 1}</td>
                    <td>{temp}</td>
                    <td>{timestamp}</td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        ) : null }
      </div>
    </div>
  )
}

Maintenance.propTypes = {
  products: PropTypes.array,
  temperatures: PropTypes.array,
  fetchTemperatures: PropTypes.func,
}

export default Maintenance
