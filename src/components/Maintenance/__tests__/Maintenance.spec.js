import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Maintenance from 'components/Maintenance'

const defaultProps = {
  products: [],
  temperatures: [],
  fetchTemperatures: jest.fn()
}

describe('<Maintenance />', () => {
  describe('@render', () => {
    it('renders with default props', () => {
      const wrapper = shallow(<Maintenance {...defaultProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('.Maintenance__products').find('table').exists()).toBeFalsy()
      expect(wrapper.find('.Maintenance__temperature').find('table').exists()).toBeFalsy()
    })
    it('renders with products', () => {
      const props = {
        ...defaultProps,
        products: [
          { product: 'coffee', stock: 30 },
          { product: 'tea', stock: 26 },
          { product: 'milk', stock: 30 },
          { product: 'sugar', stock: 30 },
        ]
      }
      const wrapper = shallow(<Maintenance {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('.Maintenance__products').find('table')).toHaveLength(1)
      expect(wrapper.find('tr.Maintenance__product')).toHaveLength(4)
    })
    it('renders with temperatures', () => {
      const props = {
        ...defaultProps,
        temperatures: [
          { timestamp: 1576668793061, temp: 99 },
          { timestamp: 1576668793100, temp: 97 },
        ]
      }
      const wrapper = shallow(<Maintenance {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('.Maintenance__temperatures').find('table')).toHaveLength(1)
      expect(wrapper.find('tr.Maintenance__temperature')).toHaveLength(2)
    })
  })

  describe('@events', () => {
    it('invokes fetchTemperatures action on button click', () => {
      const wrapper = shallow(<Maintenance {...defaultProps} />)
      expect(wrapper.find('button')).toHaveLength(1)
      expect(defaultProps.fetchTemperatures).toHaveBeenCalledTimes(0)
      wrapper.find('button').props().onClick()
      expect(defaultProps.fetchTemperatures).toHaveBeenCalledTimes(1)
    })
  })
})
