import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import { Bread } from '../Toast'

describe('<Bread />', () => {
  describe('@render', () => {
    it('renders with props', () => {
      const props = { message: `You're Toast!` }
      const wrapper = shallow(<Bread {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('.Toast__message').text()).toEqual(props.message)
    })
  })
})
