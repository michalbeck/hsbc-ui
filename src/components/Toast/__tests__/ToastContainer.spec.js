import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { createElementOnBody } from 'utils/test'

import { ToastContainer } from '../ToastContainer'
import Toast from '../Toast'

const initialProps = {
  toast: {},
  hideToast: jest.fn()
}

describe('<ToastContainer />', () => {
  describe('@render', () => {
    createElementOnBody()
    it('renders with props', () => {
      const props = {
        ...initialProps,
        toast: { message: `You're Toast!` }
      }
      const wrapper = shallow(<ToastContainer {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('Portal').exists()).toBeTruthy()
      expect(wrapper.find(Toast).exists()).toBeTruthy()
    })
    it('returns null if no props', () => {
      const wrapper = shallow(<ToastContainer />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.isEmptyRender()).toEqual(true)
    })
    it('returns null if empty toast', () => {
      const wrapper = shallow(<ToastContainer {...initialProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.isEmptyRender()).toEqual(true)
    })
  })
})
