import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import withToaster from '../Toaster'

describe('<withToaster />', () => {
  describe('@render', () => {
    it('renders', () => {
      const WrappedComponent = withToaster('<div />')
      const wrapper = shallow(<WrappedComponent />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('CSSTransition')).toHaveLength(1)
      expect(wrapper.find('CSSTransition').prop('in')).toEqual(false)
    })
  })
  describe('@behaviours', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      message: `You're Toast!`
    }
    it('transitions in => displays for 2 secs => transitions out', () => {
      jest.useFakeTimers()
      jest.spyOn(React, 'useEffect').mockImplementationOnce(f => f())
      const WrappedComponent = withToaster('<div />')
      const wrapper = shallow(<WrappedComponent {...props} />)
      expect(wrapper.find('CSSTransition').prop('in')).toEqual(true)
      jest.advanceTimersByTime(2000)
      expect(wrapper.find('CSSTransition').prop('in')).toEqual(false)
    })
    it('invokes onClose callback on transition exit', () => {
      const WrappedComponent = withToaster('<div />')
      const wrapper = shallow(<WrappedComponent {...props} />)
      wrapper.find('CSSTransition').prop('onExited')()
      expect(props.onClose).toHaveBeenCalledTimes(1)
    })
  })
})
