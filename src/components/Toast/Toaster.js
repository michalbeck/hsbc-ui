import React, { useState } from 'react'
import { CSSTransition } from 'react-transition-group'

import './Toast.css'

const withToaster = (WrappedComponent) =>
  ({ isOpen, onClose, ...props }) => {
    const [_isOpen, _setIsOpen] = useState(false)

    React.useEffect(() => {
      _setIsOpen(isOpen)
      setTimeout(() => _setIsOpen(false), 2000)
    }, [isOpen])

    return (
      <CSSTransition
        in={_isOpen}
        timeout={300}
        classNames={{
          enter: 'Toast__enter',
          enterActive: 'Toast__enter--active',
          exit: 'Toast__exit',
          exitActive: 'Toast__exit--active'
        }}
        unmountOnExit
        onExited={onClose}
      >
        <WrappedComponent {...props} />
      </CSSTransition>
    )
  }

export default withToaster
