import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'

import Toast from './Toast'

import { hideToast } from 'store/actions'
import { getToast } from 'store/selectors'

export const ToastContainer = ({ toast, hideToast }) => {
  return toast && !isEmpty(toast)
    ? ReactDOM.createPortal(
      <Toast isOpen onClose={hideToast} {...toast} />,
      document.getElementById('root')
    )
    : null
}

ToastContainer.propTypes = {
  toast: PropTypes.object
}

const ConnectedToastContainer = connect(
  (state) => ({
    toast: getToast(state)
  }),
  { hideToast }
)(ToastContainer)

export default ConnectedToastContainer
