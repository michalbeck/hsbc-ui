import React from 'react'

import withToaster from './Toaster'

import './Toast.css'

export const Bread = ({ message }) => {
  return (
    <div className='Toast__container'>
      <div className='Toast__bread'>
        <p className='Toast__message'>{message}</p>
      </div>
    </div>
  )
}

export default withToaster(Bread)
