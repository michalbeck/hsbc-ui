import React from 'react'
import { shallow, mount } from 'enzyme'
import toJson from 'enzyme-to-json'
import { MemoryRouter } from 'react-router-dom'
import { act } from 'react-dom/test-utils'

import { App } from 'components/App'

const defaultProps = {
  stock: [],
  temps: [],
  updateStock: jest.fn(),
  startInterval: jest.fn(),
  stopInterval: jest.fn(),
  fetchTemperatures: jest.fn(),
}

describe('<App />', () => {
  describe('@render', () => {
    it('renders with default props', () => {
      const wrapper = shallow(<App {...defaultProps} />)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find('NavLink')).toHaveLength(2)
      expect(wrapper.find('NavLink').at(0).props().to).toBe('/dispenser')
      expect(wrapper.find('NavLink').at(1).props().to).toBe('/maintenance')
      expect(wrapper.find('Route')).toHaveLength(3)
    })
  })

  describe('@lifecycle', () => {
    it('invokes startInterval on mount, stopInterval on unmount', () => {
      const wrapper = mount(
        <MemoryRouter>
          <App {...defaultProps} />
        </MemoryRouter>
      )
      expect(defaultProps.startInterval).toHaveBeenCalledTimes(1)
      act(() => {
        wrapper.unmount()
      })
      expect(defaultProps.stopInterval).toHaveBeenCalledTimes(1)
    })
  })
})
