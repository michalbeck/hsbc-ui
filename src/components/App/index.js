import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink, Redirect, Route, Switch } from 'react-router-dom'

import Dispenser from 'components/Dispenser'
import Maintenance from 'components/Maintenance'

import { updateStock, startInterval, stopInterval, fetchTemperatures } from 'store/actions'
import { getStock, getTemps } from 'store/selectors'

import './App.css'

export const App = ({ stock, temps, updateStock, startInterval, stopInterval, fetchTemperatures }) => {
  React.useEffect(() => {
    startInterval()
    return () => stopInterval()
  }, [startInterval, stopInterval])

  return (
    <div className='App__container'>
      <nav className='App__navbar'>
        <div className='App__navbar__links'>
          <NavLink
            className='App__navbar__link'
            activeClassName='App__navbar__link--active'
            to='/dispenser'
          >Drinks dispenser</NavLink>
          <NavLink
            className='App__navbar__link'
            activeClassName='App__navbar__link--active'
            to='/maintenance'
          >Maintenance screen</NavLink>
        </div>
      </nav>
      <main className='App__content'>
        <Switch>
          <Route exact path='/' render={() => <Redirect to='/dispenser' />} />} />
          <Route
            path='/dispenser'
            render={(props) => <Dispenser onDispense={updateStock} />}
          />
          <Route
            path='/maintenance'
            render={(props) => <Maintenance products={stock} temperatures={temps} fetchTemperatures={fetchTemperatures} />}
          />
        </Switch>
      </main>
    </div>
  )
}

App.propTypes = {
  stock: PropTypes.array,
  temps: PropTypes.array,
  updateStock: PropTypes.func,
  startInterval: PropTypes.func,
  stopInterval: PropTypes.func,
  fetchTemperatures: PropTypes.func,
}

const ConnectedApp = connect(
  (state) => ({
    stock: getStock(state),
    temps: getTemps(state),
  }),
  {
    updateStock,
    startInterval,
    stopInterval,
    fetchTemperatures,
  },
)(App)

export default ConnectedApp
