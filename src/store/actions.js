import api from 'utils/api'

const updateStock = (products) => {
  return async (dispatch, getState) => {
    const { stock } = getState().machine
    const updatedStock = stock.map((item) => {
      return products.includes(item.product)
        ? { ...item, stock: item.stock - 1 }
        : item
    })
    const lowStock = updatedStock.filter((item) => item.stock < 25)
    if (lowStock.length) dispatch(submitLowStock(lowStock))
    return dispatch({ type: 'UPDATE_STOCK', stock: updatedStock })
  }
}

const submitLowStock = (lowStock) => {
  return async (dispatch, getState) => {
    const { machine_id } = getState().machine
    const timestamp = new Date().getTime()
    try {
      const options = {
        method: 'POST',
        body: { machine_id, timestamp, stock: lowStock }
      }
      const response = await api('/lowstockalert', options)
      return dispatch(showToast({ message: response.success }))
    } catch (error) {
      console.log(error)
      return dispatch(showToast({ message: 'Unable to submit stock levels' }))
    }
  }
}

const fetchTemperatures = () => {
  return async (dispatch, getState) => {
    const { machine_id } = getState().machine
    try {
      const options = { method: 'GET' }
      const { temps } = await api(`/temperature/${machine_id}`, options)
      return dispatch({ type: 'UPDATE_TEMPS', temps })
    } catch (error) {
      console.log(error)
      return dispatch(showToast({ message: 'Unable to fetch temperatures' }))
    }
  }
}

const getRandomTemp = () => Math.floor(Math.random() * 100)/10 + 91
const submitTemperature = (temperature = getRandomTemp()) => {
  return async (dispatch, getState) => {
    const { machine_id } = getState().machine
    const timestamp = new Date().getTime()
    try {
      const options = {
        method: 'POST',
        body: { machine_id, timestamp, temperature }
      }
      const response = await api('/temperature', options)
      return dispatch(showToast({ message: response.success }))
    } catch (error) {
      console.log(error)
      return dispatch(showToast({ message: 'Unable to submit temperature' }))
    }
  }
}

const DEFAULT_INTERVAL = 60000
const startInterval = () => {
  return async (dispatch, getState) => {
    const interval = global.setInterval(() => {
      dispatch(submitTemperature())
    }, DEFAULT_INTERVAL)
    dispatch({ type: 'START_INTERVAL', interval })
  }
}

const stopInterval = () => {
  return async (dispatch, getState) => {
    const { interval } = getState().ui
    global.clearInterval(interval)
    return dispatch({ type: 'STOP_INTERVAL' })
  }
}

const showToast = (toast) => ({ type: 'SHOW_TOAST', toast })

const hideToast = () => ({ type: 'HIDE_TOAST' })

export {
  updateStock,
  submitLowStock,
  fetchTemperatures,
  submitTemperature,
  startInterval,
  stopInterval,
  showToast,
  hideToast
}