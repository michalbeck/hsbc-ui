import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import api from 'utils/api'
import * as actions from '../actions'
import * as mockData from 'mocks'

jest.mock('utils/api')

const mockStore = configureMockStore([thunk])

beforeEach(() => {
  jest.clearAllMocks()
})

// mock Date
const mockDate = new Date('2000-01-01')
beforeAll(() => {
  global.Date = class extends Date {
    constructor () { super(mockDate.getTime()) }
  }
})

global.console = { log: jest.fn() }

describe('actions', () => {
  describe('updateStock', () => {
    const { updateStock } = actions
    it('dispatches action to update stock level in store', async () => {
      const products = ['milk']
      const expectedActions = [
        { type: 'UPDATE_STOCK', stock: [{ product: 'milk', stock: 34 }]}
      ]
      const store = mockStore({
        machine: { machine_id: '123', stock: [{ product: 'milk', stock: 35 }] }
      })
      await store.dispatch(updateStock(products))
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('dispatches low stock alert if stock level < 25', async () => {
      api.mockImplementationOnce(() => Promise.resolve(mockData.success))
      const products = ['milk']
      const expectedActions = [
        { type: 'UPDATE_STOCK', stock: [{ product: 'milk', stock: 24 }]},
        { type: 'SHOW_TOAST', toast: { message: mockData.success.success }},
      ]
      const store = mockStore({
        machine: { machine_id: '123', stock: [{ product: 'milk', stock: 25 }] }
      })
      await store.dispatch(updateStock(products))
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  describe('submitLowStock', () => {
    const { submitLowStock } = actions
    it('does post request to submit low stock info', async () => {
      api.mockImplementationOnce(() => Promise.resolve(mockData.success))
      const timestamp = new Date().getTime()
      const stock = { product: 'milk', stock: 5 }
      const expectedActions = [{ type: 'SHOW_TOAST', toast: { message: mockData.success.success }}]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(submitLowStock(stock))
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/lowstockalert', {
        method: 'POST',
        body: { machine_id: '123', timestamp, stock }
      })
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('returns an error message if request fails', async () => {
      api.mockImplementationOnce(() => Promise.reject(mockData.error))
      const timestamp = new Date().getTime()
      const stock = { product: 'milk', stock: 5 }
      const expectedActions = [
        { type: 'SHOW_TOAST', toast: { message: 'Unable to submit stock levels' }}
      ]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(submitLowStock(stock))
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/lowstockalert', {
        method: 'POST',
        body: { machine_id: '123', timestamp, stock }
      })
      expect(global.console.log).toHaveBeenCalledTimes(1)
      expect(global.console.log).toHaveBeenCalledWith(mockData.error)
      expect(store.getActions()).toEqual(expectedActions)
    })
  })


  describe('fetchTemperatures', () => {
    const { fetchTemperatures } = actions
    it('does GET request to fetch latest temp readings, dispatches action to update store', async () => {
      api.mockImplementationOnce(() => Promise.resolve({ temps }))
      const temps = mockData.temperatures
      const expectedActions = [{ type: 'UPDATE_TEMPS', temps }]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(fetchTemperatures())
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/temperature/123', { method: 'GET' })
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('returns an error message if request fails', async () => {
      api.mockImplementationOnce(() => Promise.reject(mockData.error))
      const expectedActions = [{ type: 'SHOW_TOAST', toast: { message: 'Unable to fetch temperatures' }}]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(fetchTemperatures())
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/temperature/123', { method: 'GET' })
      expect(global.console.log).toHaveBeenCalledTimes(1)
      expect(global.console.log).toHaveBeenCalledWith(mockData.error)
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  describe('submitTemperature', () => {
    const { submitTemperature } = actions
    it('does POST request to submit temp reading', async () => {
      api.mockImplementationOnce(() => Promise.resolve(mockData.success))
      const timestamp = new Date().getTime()
      const expectedActions = [{ type: 'SHOW_TOAST', toast: { message: mockData.success.success }}]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(submitTemperature(99))
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/temperature', {
        method: 'POST',
        body: { machine_id: '123', timestamp, temperature: 99 }
      })
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('returns an error message if request fails', async () => {
      api.mockImplementationOnce(() => Promise.reject(mockData.error))
      const timestamp = new Date().getTime()
      const expectedActions = [{ type: 'SHOW_TOAST', toast: { message: 'Unable to submit temperature' }}]
      const store = mockStore({
        machine: { machine_id: '123' }
      })
      await store.dispatch(submitTemperature(99))
      expect(api).toHaveBeenCalledTimes(1)
      expect(api).toHaveBeenCalledWith('/temperature', {
        method: 'POST',
        body: { machine_id: '123', timestamp, temperature: 99 }
      })
      expect(global.console.log).toHaveBeenCalledTimes(1)
      expect(global.console.log).toHaveBeenCalledWith(mockData.error)
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  describe('startInterval', () => {
    const { startInterval } = actions
    it('invokes action to submit temp every interval, returns START_INTERVAL action creator', async () => {
      jest.useFakeTimers()
      api.mockImplementation(() => Promise.resolve({}))
      const expectedActions = [{ type: 'START_INTERVAL', interval: 1 }]
      const store = mockStore({})
      expect(setInterval).toHaveBeenCalledTimes(0)
      await store.dispatch(startInterval())
      expect(setInterval).toHaveBeenCalledTimes(1)
      expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 60000)
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  describe('stopInterval', () => {
    const { stopInterval } = actions
    it('clears interval and returns STOP_INTERVAL action creator', async () => {
      jest.useFakeTimers()
      const expectedActions = [{ type: 'STOP_INTERVAL' }]
      const store = mockStore({
        ui: { interval: 123 }
      })
      expect(clearInterval).toHaveBeenCalledTimes(0)
      await store.dispatch(stopInterval())
      expect(clearInterval).toHaveBeenCalledTimes(1)
      expect(clearInterval).toHaveBeenCalledWith(123)
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  describe('showToast', () => {
    it('returns SHOW_TOAST action creator with toast state', () => {
      const { showToast } = actions
      const toast = { message: `You're Toast!` }
      expect(showToast(toast)).toEqual({ type: 'SHOW_TOAST', toast })
    })
  })

  describe('hideToast', () => {
    it('returns HIDE_TOAST action creator', () => {
      const { hideToast } = actions
      expect(hideToast()).toEqual({ type: 'HIDE_TOAST' })
    })
  })
})