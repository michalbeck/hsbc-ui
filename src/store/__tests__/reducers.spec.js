import reducer from '../reducers'

describe('reducer', () => {
  it('handles UPDATE_STOCK action', () => {
    const stock = [{ product: 'tea', stock: 9 }]
    const initialState = {
      machine: { stock: [] },
      ui: {}
    }
    const action = { type: 'UPDATE_STOCK', stock }
    expect(reducer(initialState, action)).toEqual({
      machine: { stock },
      ui: {}
    })
  })

  it('handles UPDATE_TEMPS action', () => {
    const temps = [{ timestamp: 1, temp: 99.2 }]
    const initialState = {
      machine: { temps: [] },
      ui: {}
    }
    const action = { type: 'UPDATE_TEMPS', temps }
    expect(reducer(initialState, action)).toEqual({
      machine: { temps },
      ui: {}
    })
  })

  it('handles START_INTERVAL action', () => {
    const interval = 123
    const initialState = {
      machine: {},
      ui: { interval: null }
    }
    const action = { type: 'START_INTERVAL', interval }
    expect(reducer(initialState, action)).toEqual({
      machine: {},
      ui: { interval }
    })
  })

  it('handles STOP_INTERVAL action', () => {
    const interval = 123
    const initialState = {
      machine: {},
      ui: { interval }
    }
    const action = { type: 'STOP_INTERVAL' }
    expect(reducer(initialState, action)).toEqual({
      machine: {},
      ui: { interval: null }
    })
  })  
  
  it('handles SHOW_TOAST action', () => {
    const toast = { message: `You're Toast!` }
    const initialState = {
      machine: {},
      ui: { toast: {} }
    }
    const action = { type: 'SHOW_TOAST', toast }
    expect(reducer(initialState, action)).toEqual({
      machine: {},
      ui: { toast }
    })
  })

  it('handles HIDE_TOAST action', () => {
    const toast = { message: `You're Toast!` }
    const initialState = {
      machine: {},
      ui: { toast }
    }
    const action = { type: 'HIDE_TOAST' }
    expect(reducer(initialState, action)).toEqual({
      machine: {},
      ui: { toast: {} }
    })
  })
})
