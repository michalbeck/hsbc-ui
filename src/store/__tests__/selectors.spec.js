import * as selectors from '../selectors'

describe('selectors', () => {
  describe('getStock', () => {
    const { getStock } = selectors
    it('returns default value when stock state does not exist or is falsy', () => {
      const expectedValue = []
      expect(getStock({})).toEqual(expectedValue)
      expect(getStock({ machine: undefined })).toEqual(expectedValue)
      expect(getStock({ machine: null })).toEqual(expectedValue)
      expect(getStock({ machine: {} })).toEqual(expectedValue)
      expect(getStock({ machine: { stock: null } })).toEqual(expectedValue)
      expect(getStock({ machine: { stock: undefined } })).toEqual(expectedValue)
    })
    it('returns stock state when exists in store', () => {
      const expectedValue = [{ product: 'tea', stock: 10 }]
      const state = { machine: { stock: [{ product: 'tea', stock: 10 }] }}
      expect(getStock(state)).toEqual(expectedValue)
    })
  })
  describe('getTemps', () => {
    const { getTemps } = selectors
    it('returns default value when temps state does not exist or is falsy', () => {
      const expectedValue = []
      expect(getTemps({})).toEqual(expectedValue)
      expect(getTemps({ machine: undefined })).toEqual(expectedValue)
      expect(getTemps({ machine: null })).toEqual(expectedValue)
      expect(getTemps({ machine: {} })).toEqual(expectedValue)
      expect(getTemps({ machine: { temps: null } })).toEqual(expectedValue)
      expect(getTemps({ machine: { temps: undefined } })).toEqual(expectedValue)
    })
    it('returns stock state when exists in store', () => {
      const expectedValue = [{ timestamp: 1, temp: 99 }]
      const state = { machine: { temps: [{ timestamp: 1, temp: 99 }] }}
      expect(getTemps(state)).toEqual(expectedValue)
    })
  })
  describe('getToast', () => {
    const { getToast } = selectors
    it('returns default value when toast state does not exist or is falsy', () => {
      const expectedValue = {}
      expect(getToast({})).toEqual(expectedValue)
      expect(getToast({ ui: {} })).toEqual(expectedValue)
    })
    it('returns toast state when exists in store', () => {
      const expectedValue = { message: `You're toast!` }
      const state = { ui: { toast: { message: `You're toast!` } } }
      expect(getToast(state)).toEqual(expectedValue)
    })
  })
})
