import { combineReducers } from 'redux'
import { createReducer } from 'utils/store'

const machineReducer = createReducer(
  {
    machine_id: '123',
    stock: [
      { product: 'coffee', stock: 30 },
      { product: 'tea', stock: 26 },
      { product: 'milk', stock: 30 },
      { product: 'sugar', stock: 30 },
    ],
    temps: [],
  },
  {
    UPDATE_STOCK: (state, { stock }) => ({
      ...state,
      stock,
    }),
    UPDATE_TEMPS: (state, { temps }) => ({
      ...state,
      temps,
    }),
  },
)

const uiReducer = createReducer(
  {
    interval: null,
    toast: {}
  },
  {
    START_INTERVAL: (state, { interval }) => ({
      ...state,
      interval
    }),
    STOP_INTERVAL: (state) => ({
      ...state,
      interval: null
    }),
    SHOW_TOAST: (state, { toast }) => ({
      ...state,
      toast
    }),
    HIDE_TOAST: (state) => ({
      ...state,
      toast: {}
    })
  }
)

const rootReducer = combineReducers({
  machine: machineReducer,
  ui: uiReducer
})

export default rootReducer
