import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const middleware = [
  thunk,
  // add other middleware if required eg sagas
]

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose

const configureStore = () =>
  createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(...middleware),
      // add other store enhancers if required
    ),
  )

export default configureStore
