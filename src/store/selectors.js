import { pathOr } from 'ramda'

const getStock = (state) => pathOr([], ['machine', 'stock'], state)
const getTemps = (state) => pathOr([], ['machine', 'temps'], state)
const getToast = (state) => pathOr({}, ['ui', 'toast'], state)

export {
  getStock,
  getTemps,
  getToast
}