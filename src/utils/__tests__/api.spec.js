import apiFetch from 'utils/api'

import * as mockData from 'mocks'

beforeEach(() => {
  jest.resetAllMocks()
})

describe('api', () => {
  describe('apiFetch', () => {
    it('makes request with response status 200', async () => {
      global.fetch = jest.fn()
        .mockImplementationOnce(() => Promise.resolve({
          json: () => Promise.resolve({ status: 200, body: { test: 'test' }})
        }))
      const expectedUrl = 'http://localhost:3001/api/test-path'
      const expectedOptions = { method: 'GET' }
      const response = await apiFetch(`/test-path`, { method: 'GET' }, true)
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toBeCalledWith(expectedUrl, expectedOptions)
      expect(response).toEqual(expect.objectContaining({
        status: expect.any(Number),
        body: expect.any(Object)
      }))
      expect(response).toEqual({ status: 200, body: { test: 'test' }})
    })

    it('makes request with response status 204', async () => {
      global.fetch = jest.fn()
        .mockImplementationOnce(() => Promise.resolve({
          json: () => Promise.resolve({ status: 204, body: { test: 'test' }})
        }))
      const expectedUrl = 'http://localhost:3001/api/test-path'
      const expectedOptions = { method: 'GET' }
      const response = await apiFetch(`/test-path`, { method: 'GET' }, true)
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toBeCalledWith(expectedUrl, expectedOptions)
      expect(response).toEqual(expect.objectContaining({
        status: expect.any(Number),
        body: expect.any(Object)
      }))
      expect(response).toEqual({ status: 204, body: { test: 'test' }})
    })

    it('makes request with response status 500', async () => {
      global.fetch = jest.fn()
        .mockImplementationOnce(() => Promise.resolve({
          json: () => Promise.resolve({ status: 500, body: { test: 'test' }})
        }))
      const expectedUrl = 'http://localhost:3001/api/test-path'
      const expectedOptions = { method: 'GET' }
      try {
        await apiFetch(`/test-path`, { method: 'GET' }, true)
      } catch (e) {
        expect(e).toEqual(Error('500 undefined'))
      }
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toBeCalledWith(expectedUrl, expectedOptions)
    })

    it('makes request with response status 401', async () => {
      global.fetch = jest.fn()
        .mockImplementationOnce(() => Promise.resolve({
          json: () => Promise.resolve({ status: 401, body: { test: 'test' }})
        }))
      const expectedUrl = 'http://localhost:3001/api/test-path'
      const expectedOptions = { method: 'GET' }
      try {
        await apiFetch(`/test-path`, { method: 'GET' }, true)
      } catch (e) {
        expect(e).toEqual(Error('401 undefined'))
      }
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toBeCalledWith(expectedUrl, expectedOptions)
    })

    it('makes request with response status 404', async () => {
      global.fetch = jest.fn()
        .mockImplementationOnce(() => Promise.resolve({
          json: () => Promise.resolve({ status: 404, body: { test: 'test' }})
        }))
      const expectedUrl = 'http://localhost:3001/api/test-path'
      const expectedOptions = { method: 'GET' }
      try {
        await apiFetch(`/test-path`, { method: 'GET' }, true)
      } catch (e) {
        expect(e).toEqual(Error('404 undefined'))
      }
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toBeCalledWith(expectedUrl, expectedOptions)
    })
  })
})
