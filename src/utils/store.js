const createReducer = (initialState, handlers) => (state, action) => {
  const newState = state === undefined ? initialState : state
  const { type } = action
  if (handlers[type]) {
    return handlers[type](newState, action)
  }
  return newState
}

export { createReducer }
