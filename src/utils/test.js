const createElementOnBody = (id = 'root') => {
  const div = global.document.createElement('div')
  div.setAttribute('id', id)
  const body = global.document.querySelector('body')
  body.appendChild(div)
}

export {
  createElementOnBody
}
