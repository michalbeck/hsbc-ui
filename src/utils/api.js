const path = 'http://localhost:3001/api'

const apiFetch = async (endpoint, options) => {
  const requestOptions = {
    ...options
  }
  if (options.body) {
    requestOptions.headers = { 'Content-Type': 'application/json; charset=utf-8' }
    requestOptions.body = JSON.stringify(options.body)
  }

  return window.fetch(`${path}${endpoint}`, requestOptions)
    .then(response => response.json())
    .catch(error => {
      console.log('api error', error)
      throw error
    })
}

export default apiFetch